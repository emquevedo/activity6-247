//Emily Quevedo
//April 2, 2019
//In Class Activity 6
//This is my own work
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Activity3.Models;
using Activity3.Services.Business;

namespace Activity3.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        [HttpGet]
        public ActionResult Index()
        {
            return View("Login");
        }

        [HttpPost]
        public ActionResult Login(UserModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Login");
            }

            SecurityService services = new SecurityService();
            try
            {
                //call security service to authenticate the user
                SecurityService service = new SecurityService();
                bool result = service.Authenticate(model);

                if (result)
                {
                    return View("LoginPassed", model);
                }
                else
                {
                    return View("LoginFailed");
                }
            }

            catch (Exception)
            {
                return View("LoginError");
            }
        }
    }
}